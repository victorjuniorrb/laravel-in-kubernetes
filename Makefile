# VERSION defines the version for the docker containers.
# To build a specific set of containers with a version,
# you can use the VERSION as an arg of the docker build command (e.g make docker VERSION=0.0.2)
VERSION ?= v0.0.1

# REGISTRY defines the registry where we store our images.
# To push to a specific registry,
# you can use the REGISTRY as an arg of the docker build command (e.g make docker REGISTRY=my_registry.com/username)
# You may also change the default value if you are using a different registry as a default
REGISTRY ?= registry.gitlab.com/victorjuniorrb/laravel-in-kubernetes

# Commands
docker: docker-build docker-push

docker-build:
	docker build . --target cli -t ${REGISTRY}/cli:${VERSION}
	docker build . --target cron -t ${REGISTRY}/cron:${VERSION}
	docker build . --target fpm_server -t ${REGISTRY}/fpm_server:${VERSION}
	docker build . --target web_server -t ${REGISTRY}/web_server:${VERSION}

docker-arm64:
	docker buildx build --target cli --platform linux/amd64,linux/arm64 -t ${REGISTRY}/cli:${VERSION} --push .
	docker buildx build --target cron --platform linux/amd64,linux/arm64 -t ${REGISTRY}/cron:${VERSION} --push .
	docker buildx build --target fpm_server --platform linux/amd64,linux/arm64 -t ${REGISTRY}/fpm_server:${VERSION} --push .
	docker buildx build --target web_server --platform linux/amd64,linux/arm64 -t ${REGISTRY}/web_server:${VERSION} --push .

docker-push:
	docker push ${REGISTRY}/cli:${VERSION}
	docker push ${REGISTRY}/cron:${VERSION}
	docker push ${REGISTRY}/fpm_server:${VERSION}
	docker push ${REGISTRY}/web_server:${VERSION}
